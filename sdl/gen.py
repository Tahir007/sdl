
from .args import IntArg, FloatArg, Vec2Arg, Vec3Arg, Vec4Arg


class AsmGenerator:
    def __init__(self, AVX=False, BIT64=True, SSE41=True):
        self.avx = AVX
        self.bit64 = BIT64
        self.sse41 = SSE41

    def load(self, dst_reg, typ, name=None, ptr_reg=None, offset=None):
        # 1. check compatability of typ with dest_reg

        if name is None and ptr_reg is None:
            raise ValueError('Could not load beacuse both name and ptr_reg are None!')

        source = name if name is not None else ptr_reg
        offset = ' + %i' % offset if offset is not None else ''

        if typ == IntArg:
            code = "mov %s, dword [%s%s] \n" % (dst_reg, source, offset)
        elif typ == FloatArg:
            if self.avx:
                code = "vmovss %s, dword [%s%s] \n" % (dst_reg, source, offset)
            else:
                code = "movss %s, dword [%s%s] \n" % (dst_reg, source, offset)
        elif typ in (Vec2Arg, Vec3Arg, Vec4Arg):
            if self.avx:
                code = "vmovaps %s, oword[%s%s]\n" % (dst_reg, source, offset)
            else:
                code = "movaps %s, oword[%s%s]\n" % (dst_reg, source, offset)
        else:
            raise ValueError('Unsupported type for load!', typ)
        return code

    def store(self, src_reg, typ, name=None, ptr_reg=None, offset=None):

        if name is None and ptr_reg is None:
            raise ValueError('Could not store beacuse both name and ptr_reg are None!')

        source = name if name is not None else ptr_reg
        offset = ' + %i' % offset if offset is not None else ''

        if typ == IntArg:
            code = "mov dword [%s%s], %s \n" % (source, offset, src_reg)
        elif typ == FloatArg:
            if self.avx:
                code = "vmovss dword [%s%s], %s \n" % (source, offset, src_reg)
            else:
                code = "movss dword [%s%s], %s \n" % (source, offset, src_reg)
        elif typ in (Vec2Arg, Vec3Arg, Vec4Arg):
            if self.avx:
                code = "vmovaps oword[%s%s], %s\n" % (source, offset, src_reg)
            else:
                code = "movaps oword[%s%s], %s\n" % (source, offset, src_reg)
        else:
            raise ValueError('Unsupported type for store!', typ)
        return code

    def load_attribute(self, ptr_reg, path, typ, dst_reg):
        # 1. TODO check compatability of typ with dest_reg
        if typ == IntArg:
            code = "mov %s, dword [%s + %s]\n" % (dst_reg, ptr_reg, path)
        elif typ == FloatArg:
            if self.avx:
                code = "vmovss %s, dword [%s + %s]\n" % (dst_reg, ptr_reg, path)
            else:
                code = "movss %s, dword [%s + %s]\n" % (dst_reg, ptr_reg, path)
        elif typ in (Vec2Arg, Vec3Arg, Vec4Arg):
            if self.avx:
                code = "vmovaps %s, oword [%s + %s]\n" % (dst_reg, ptr_reg, path)
            else:
                code = "movaps %s, oword [%s + %s]\n" % (dst_reg, ptr_reg, path)
        else:
            raise ValueError('Unsupported attribute type!', path, typ)
        return code

    def store_attribute(self, ptr_reg, path, typ, src_reg):
        if typ == IntArg:
            code = "mov dword [%s + %s], %s\n" % (ptr_reg, path, src_reg)
        elif typ == FloatArg:
            if self.avx:
                code = "vmovss dword [%s + %s], %s\n" % (ptr_reg, path, src_reg)
            else:
                code = "movss dword [%s + %s], %s\n" % (ptr_reg, path, src_reg)
        return code

    def move(self, src_reg, typ, dst_reg):
        # 1. TODO check compatability of typ with dest_reg
        if typ == IntArg:
            code = 'mov %s, %s\n' % (dst_reg, src_reg)
        elif typ in (FloatArg, Vec2Arg, Vec3Arg, Vec4Arg):
            if self.avx:
                code = "vmovaps %s, %s \n" % (dst_reg, src_reg)
            else:
                code = "movaps %s, %s \n" % (dst_reg, src_reg)
        else:
            raise ValueError('Unsupported type for move!', typ)
        return code

    def abs(self, src_reg, typ, tmp_reg):
        # 1. TODO check compatability of typ with dest_reg
        if typ == IntArg:
            raise ValueError('Not yet implemented abs in generator for int type')
        elif typ in (FloatArg, Vec2Arg, Vec3Arg, Vec4Arg):
            if self.avx:
                code = 'vpcmpeqw %s, %s, %s\n' % (tmp_reg, tmp_reg, tmp_reg)
                code += 'vpsrld %s, %s, 1\n' % (tmp_reg, tmp_reg)
                code += 'vandps %s, %s, %s\n' % (src_reg, src_reg, tmp_reg)
            else:
                code = 'pcmpeqw %s, %s\n' % (tmp_reg, tmp_reg)
                code += 'psrld %s, 1\n' % tmp_reg
                code += 'andps %s, %s\n' % (src_reg, tmp_reg)
        else:
            raise ValueError('Unsupported type for calc of absolute!', typ)
        return code

    def max(self, dst_reg, typ, src_reg):
        # 1. TODO check compatability of typ with dest_reg
        if typ == IntArg:
            code = 'cmp %s, %s\n' % (dst_reg, src_reg)
            code += 'cmovl %s, %s\n' % (dst_reg, src_reg)
        elif typ == FloatArg:
            if self.avx:
                code = 'vmaxss %s, %s, %s\n' % (dst_reg, dst_reg, src_reg)
            else:
                code = 'maxss %s, %s\n' % (dst_reg, src_reg)
        elif typ in (Vec2Arg, Vec3Arg, Vec4Arg):
            if self.avx:
                code = 'vmaxps %s, %s, %s\n' % (dst_reg, dst_reg, src_reg)
            else:
                code = 'maxps %s, %s\n' % (dst_reg, src_reg)
        else:
            raise ValueError('Unsupported type!', typ)
        return code

    def min(self, dst_reg, typ, src_reg):
        # 1. TODO check compatability of typ with dest_reg
        if typ == IntArg:
            code = 'cmp %s, %s\n' % (dst_reg, src_reg)
            code += 'cmovg %s, %s\n' % (dst_reg, src_reg)
        elif typ == FloatArg:
            if self.avx:
                code = 'vminss %s, %s, %s\n' % (dst_reg, dst_reg, src_reg)
            else:
                code = 'minss %s, %s\n' % (dst_reg, src_reg)
        elif typ in (Vec2Arg, Vec3Arg, Vec4Arg):
            if self.avx:
                code = 'vminps %s, %s, %s\n' % (dst_reg, dst_reg, src_reg)
            else:
                code = 'minps %s, %s\n' % (dst_reg, src_reg)
        else:
            raise ValueError('Unsupported type!', typ)
        return code

    def ceil(self, dst_reg, typ, src_reg, tmp_reg, tmp2_reg, tmp3_reg):
        # 1. TODO check compatability of typ with dest_reg

        if typ != FloatArg:
            raise ValueError('Only float type for now!', typ)

        if self.avx:
            code = 'vroundss %s, %s, %s, 2\n' % (tmp_reg, tmp_reg, src_reg)
            code += "vcvttss2si %s, %s \n" % (dst_reg, tmp_reg)
        else:
            # TODO: SSE41 implementation missing!!!!

            # NOTE: This code could handle also vec2, vec3, and vec4 type 
            code = 'cvttps2dq %s, %s\n' % (tmp_reg, src_reg)
            code += 'pcmpeqw %s,%s\n' % (tmp3_reg, tmp3_reg)
            code += 'pslld %s,25\n' % tmp3_reg
            code += 'psrld %s,2\n' % tmp3_reg
            code += 'cvtdq2ps %s, %s\n' % (tmp_reg, tmp_reg)
            code += 'movaps %s, %s\n' % (tmp2_reg, tmp_reg)
            code += 'cmpps %s, %s, 1\n' % (tmp2_reg, src_reg)
            code += 'andps %s, %s\n' % (tmp3_reg, tmp2_reg)
            code += 'addps %s, %s\n' % (tmp3_reg, tmp_reg)
            code += 'cvttss2si %s, %s \n' % (dst_reg, tmp3_reg)
        return code

    def floor(self, dst_reg, typ, src_reg, tmp_reg, tmp2_reg, tmp3_reg):
        # 1. TODO check compatability of typ with dest_reg

        if typ != FloatArg:
            raise ValueError('Only float type for now!', typ)

        if self.avx:
            code = 'vroundss %s, %s, %s, 1\n' % (tmp_reg, tmp_reg, src_reg)
            code += "vcvttss2si %s, %s \n" % (dst_reg, tmp_reg)
        else:
            # TODO: SSE41 implementation missing!!!!

            # NOTE: This code could handle also vec2, vec3, and vec4 type 
            code = 'cvttps2dq %s, %s\n' % (tmp_reg, src_reg)
            code += 'pcmpeqw %s,%s\n' % (tmp3_reg, tmp3_reg)
            code += 'pslld %s,25\n' % tmp3_reg
            code += 'psrld %s,2\n' % tmp3_reg
            code += 'cvtdq2ps %s, %s\n' % (tmp_reg, tmp_reg)
            code += 'movaps %s, %s\n' % (tmp2_reg, tmp_reg)
            code += 'cmpps %s, %s, 6\n' % (tmp2_reg, src_reg)
            code += 'andps %s, %s\n' % (tmp3_reg, tmp2_reg)
            code += 'subps %s, %s\n' % (tmp_reg, tmp3_reg)
            code += 'cvttss2si %s, %s \n' % (dst_reg, tmp_reg)
        return code

    def generate_float_one(self, xmm):
        """ Generate float 1.0 in xmm register"""

        if self.avx:
            code = 'vpcmpeqw %s, %s, %s\n' % (xmm, xmm, xmm)
            code += 'vpslld %s, %s, 25\n' % (xmm, xmm)
            code += 'vpsrld %s, %s, 2\n' % (xmm, xmm)
        else:
            code = 'pcmpeqw %s, %s\n' % (xmm, xmm)
            code += 'pslld %s, 25\n' % xmm
            code += 'psrld %s, 2\n' % xmm
        return code
