
from .args import IntArg, Vec3Arg, Vec2Arg, StructArg, FloatArg
from .cgen import register_function
from .asm_cmds import load_operand


def _ray_bbox_isect(cgen, operands):
    if len(operands) != 4:
        msg = "Wrong number of arguments in ray_bbox_intersect function."
        raise ValueError(msg, operands)

    # arguments, inv_direction, origin, bbox_min, bbox_max
    code0, xmm0, typ0 = load_operand(cgen, operands[0])
    code1, xmm1, typ1 = load_operand(cgen, operands[1])
    code2, xmm2, typ2 = load_operand(cgen, operands[2])
    code3, xmm3, typ3 = load_operand(cgen, operands[3])
    xmm4 = cgen.register(typ='xmm')
    xmm5 = cgen.register(typ='xmm')
    reg = cgen.register(typ='general')
    reg2 = cgen.register(typ='general')

    if cgen.AVX:
        code = code0 + code1 + code2 + code3
        code += 'vblendvps %s, %s, %s, %s\n' % (xmm4, xmm2, xmm3, xmm0)
        code += "vsubps %s, %s, %s\n" % (xmm4, xmm4, xmm1)
        code += "vmulps %s, %s, %s\n" % (xmm4, xmm4, xmm0)

        code += 'vpcmpeqw %s, %s, %s\n' % (xmm5, xmm5, xmm5)
        code += "vpxor %s, %s, %s\n" % (xmm5, xmm5, xmm0)
        code += 'vblendvps %s, %s, %s, %s\n' % (xmm5, xmm2, xmm3, xmm5)
        code += "vsubps %s, %s, %s\n" % (xmm5, xmm5, xmm1)
        code += "vmulps %s, %s, %s\n" % (xmm5, xmm5, xmm0)

        code += 'vpermilps %s, %s, 0x2\n' % (xmm0, xmm4)
        code += 'vmaxss %s, %s, %s\n' % (xmm0, xmm0, xmm4)
        code += 'vpermilps %s, %s, 0x1\n' % (xmm1, xmm4)
        code += 'vmaxss %s, %s, %s\n' % (xmm0, xmm0, xmm1)

        code += 'vpermilps %s, %s, 0x2\n' % (xmm1, xmm5)
        code += 'vminss %s, %s, %s\n' % (xmm1, xmm1, xmm5)
        code += 'vpermilps %s, %s, 0x1\n' % (xmm2, xmm5)
        code += 'vminss %s, %s, %s\n' % (xmm1, xmm1, xmm2)
        # TODO multiply xmm1 * 1.00000024f for numerical robustness

        code += 'vinsertps %s, %s, %s, 0x10\n' % (xmm0, xmm0, xmm1)

    elif cgen.SSE41:
        # TODO improve usage of implict mask xmm0
        # NOTE xmm0 improve this, error will ocur if inv_dir is not in xmm0!!!

        xmm6 = cgen.register(typ='xmm')
        code = code0 + code1 + code2 + code3
        if xmm0 != 'xmm0':
            raise ValueError("Not yet solved ray box isect! Fix this!!!!!")
            # code += 'movaps %s, %s\n' % (xmm6, xmm0)
            # xmm0, xmm_ = 'xmm0', 'xmm_'

        code += 'movaps %s, %s\n' % (xmm4, xmm2)
        code += 'blendvps %s, %s\n' % (xmm4, xmm3)
        code += 'subps %s, %s\n' % (xmm4, xmm1)
        code += 'mulps %s, %s\n' % (xmm4, xmm0)

        code += 'movaps %s, %s\n' % (xmm6, xmm0)
        code += 'pcmpeqw %s, %s\n' % (xmm5, xmm5)
        code += 'pxor %s, %s\n' % (xmm0, xmm5)
        code += 'movaps %s, %s\n' % (xmm5, xmm2)
        code += 'blendvps %s, %s\n' % (xmm5, xmm3)
        code += 'subps %s, %s\n' % (xmm5, xmm1)
        code += 'mulps %s, %s\n' % (xmm5, xmm6)

        code += 'insertps %s, %s, 0x40\n' % (xmm0, xmm4)
        code += 'maxss %s, %s\n' % (xmm0, xmm4)
        code += 'insertps %s, %s, 0x80\n' % (xmm1, xmm4)
        code += 'maxss %s, %s\n' % (xmm0, xmm1)

        code += 'insertps %s, %s, 0x40\n' % (xmm2, xmm5)
        code += 'minss %s, %s\n' % (xmm2, xmm5)
        code += 'insertps %s, %s, 0x80\n' % (xmm1, xmm5)
        code += 'minss %s, %s\n' % (xmm2, xmm1)
        # TODO multiply xmm2 * 1.00000024f for numerical robustness

        code += 'insertps %s, %s, 0x10\n' % (xmm0, xmm2)

        cgen.release_reg(xmm6)
    else:
        raise ValueError("Only sse4.1 and avx are supported in ray_bbox_isect.")

    cgen.release_reg(reg)
    cgen.release_reg(reg2)
    cgen.release_reg(xmm1)
    cgen.release_reg(xmm2)
    cgen.release_reg(xmm3)
    cgen.release_reg(xmm4)
    cgen.release_reg(xmm5)
    return code, xmm0, Vec2Arg


register_function('ray_bbox_isect', _ray_bbox_isect, inline=True)


def _precompute_watertight(cgen, operands):
    if len(operands) != 1:
        msg = "Wrong number of arguments in precompute watertight function."
        raise ValueError(msg, operands)
    code, reg, arg = load_operand(cgen, operands[0])
    if not isinstance(arg, StructArg) or arg.type_name != 'Ray':
        raise ValueError("Ray structure is expected.", arg)

    dir_reg = cgen.register(typ='xmm')
    xmm1 = cgen.register(typ='xmm')
    xmm2 = cgen.register(typ='xmm')
    xmm3 = cgen.register(typ='xmm')
    reg2 = cgen.register(typ='general')
    reg3 = cgen.register(typ='general')
    reg4 = cgen.register(typ='pointer')

    code += cgen.gen.load_attribute(reg, 'Ray.direction', Vec3Arg, dir_reg)
    code += cgen.gen.move(dir_reg, Vec3Arg, xmm1)
    code += cgen.gen.abs(src_reg=xmm1, typ=Vec3Arg, tmp_reg=xmm2)

    if cgen.AVX:
        code += "vmovaps %s, %s \n" % (xmm2, xmm1)
        code += 'vshufps %s, %s, %s, 0x1\n' % (xmm2, xmm2, xmm2)
        code += 'vmovhlps %s, %s, %s\n' % (xmm3, xmm3, xmm1)
    else:
        code += "movaps %s, %s \n" % (xmm2, xmm1)
        code += 'shufps %s, %s, 0x1\n' % (xmm2, xmm2)
        code += 'movhlps %s, %s\n' % (xmm3, xmm1)

    code += cgen.gen.max(dst_reg=xmm1, typ=FloatArg, src_reg=xmm2)
    code += cgen.gen.max(dst_reg=xmm1, typ=FloatArg, src_reg=xmm3)
    code += 'mov %s, 0\n' % reg2
    code += 'mov %s, 4\n' % reg3
    comiss = 'vucomiss' if cgen.AVX else 'ucomiss'
    code += '%s %s, %s\n' % (comiss, xmm1, xmm2)
    code += 'cmove %s, %s\n' % (reg2, reg3)
    code += 'mov %s, 8\n' % reg3
    code += '%s %s, %s\n' % (comiss, xmm1, xmm3)
    code += 'cmove %s, %s\n' % (reg2, reg3)
    code += cgen.gen.store_attribute(reg, 'Ray._kz', IntArg, reg2)
    code += 'add %s, 4\n' % reg2
    code += 'mov %s, 0\n' % reg3
    code += 'cmp %s, 12\n' % reg2
    code += 'cmove %s, %s\n' % (reg2, reg3)
    code += cgen.gen.store_attribute(reg, 'Ray._kx', IntArg, reg2)
    code += 'add %s, 4\n' % reg2
    code += 'cmp %s, 12\n' % reg2
    code += 'cmove %s, %s\n' % (reg2, reg3)
    code += cgen.gen.store_attribute(reg, 'Ray._ky', IntArg, reg2)

    label_swap = 'end_swap_%i' % id(operands)

    if cgen.BIT64:
        code += 'mov %s, rsp\n' % reg4
        code += 'and %s, 0xFFFFFFF0\n' % reg4
        if cgen.AVX:
            code += 'vmovaps oword [%s - 16], %s\n' % (reg4, dir_reg)
        else:
            code += 'movaps oword [%s - 16], %s\n' % (reg4, dir_reg)
        code += cgen.gen.load_attribute(reg, 'Ray._kz', IntArg, reg2)
        if cgen.AVX:
            code += 'vmovss %s, dword [%s + %s - 16]\n' % (xmm1, reg4, 'r' + reg2[1:])
            code += 'vpxor %s, %s, %s\n' % (xmm2, xmm2, xmm2)
        else:
            code += 'movss %s, dword [%s + %s - 16]\n' % (xmm1, reg4, 'r' + reg2[1:])
            code += 'pxor %s, %s\n' % (xmm2, xmm2)
        code += '%s %s, %s\n' % (comiss, xmm2, xmm1)
        code += 'jc %s\n' % label_swap
        code += cgen.gen.load_attribute(reg, 'Ray._kx', IntArg, reg2)
        code += cgen.gen.load_attribute(reg, 'Ray._ky', IntArg, reg3)
        code += cgen.gen.store_attribute(reg, 'Ray._kx', IntArg, reg3)
        code += cgen.gen.store_attribute(reg, 'Ray._ky', IntArg, reg2)
        code += '%s:\n' % label_swap
        code += cgen.gen.load_attribute(reg, 'Ray._kx', IntArg, reg2)
        if cgen.AVX:
            code += 'vmovss %s, dword [%s + %s - 16]\n' % (xmm2, reg4, 'r' + reg2[1:])
            code += 'vdivss %s, %s, %s\n' % (xmm2, xmm2, xmm1)
        else:
            code += 'movss %s, dword [%s + %s - 16]\n' % (xmm2, reg4, 'r' + reg2[1:])
            code += 'divss %s, %s\n' % (xmm2, xmm1)
        code += cgen.gen.store_attribute(reg, 'Ray._sx', FloatArg, xmm2)
        code += cgen.gen.load_attribute(reg, 'Ray._ky', IntArg, reg3)
        if cgen.AVX:
            code += 'vmovss %s, dword [%s + %s - 16]\n' % (xmm3, reg4, 'r' + reg3[1:])
            code += 'vdivss %s, %s, %s\n' % (xmm3, xmm3, xmm1)
        else:
            code += 'movss %s, dword [%s + %s - 16]\n' % (xmm3, reg4, 'r' + reg3[1:])
            code += 'divss %s, %s\n' % (xmm3, xmm1)
        code += cgen.gen.store_attribute(reg, 'Ray._sy', FloatArg, xmm3)
        code += cgen.gen.generate_float_one(xmm2)  # generate constant 1.0
        if cgen.AVX:
            code += 'vdivss %s, %s, %s\n' % (xmm2, xmm2, xmm1)
        else:
            code += 'divss %s, %s\n' % (xmm2, xmm1)
        code += cgen.gen.store_attribute(reg, 'Ray._sz', FloatArg, xmm2)
    else:
        raise ValueError("32 bit is not supported yet.")

    cgen.release_reg(dir_reg)
    cgen.release_reg(xmm1)
    cgen.release_reg(xmm2)
    cgen.release_reg(xmm3)
    cgen.release_reg(reg2)
    cgen.release_reg(reg3)
    cgen.release_reg(reg4)

    return code, reg, IntArg


register_function('precompute_ray_watertight', _precompute_watertight, inline=True)
