from .vector import Vector3

class Ray:

    __slots__ = ['origin', 'direction', '_kx', '_ky', '_kz', '_sx', '_sy', '_sz']

    def __init__(self, origin, direction):
        self.origin = origin
        self.direction = direction
        self._kx = 0
        self._ky = 0
        self._kz = 0
        self._sx = 1.0
        self._sy = 1.0
        self._sz = 1.0

    def __str__(self):
        return "Origin = {0}\nDirection = {1}".format(self.origin, self.direction)

    @classmethod
    def factory(cls):
        origin = Vector3(0.0, 0.0, 0.0)
        direction = Vector3(0.0, 0.0, 1.0)
        return Ray(origin, direction)
