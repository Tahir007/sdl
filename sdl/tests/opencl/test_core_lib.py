
from sdl.opencl.core import get_platform_ids, CLPlatformInfo, CLDeviceInfo,\
    get_default_device_ids, CLContext

def display_pl_info(platform_id):
    pi = CLPlatformInfo(platform_id)
    print(pi.name)
    print(pi.version)
    print(pi.vendor)
    print(pi.extensions)
    print(pi.profile)

def display_device_info(device_id):
    di = CLDeviceInfo(device_id)
    #print(di.type)
    print('Vendor ID', di.vendor_id)
    print('Max compute units', di.max_compute_units)
    print('Max work item dimensions', di.max_work_item_dims)
    print('Preferred vec width char', di.preferred_vector_width_char)
    print('Preferred vec width short', di.preferred_vector_width_short)
    print('Preferred vec width int', di.preferred_vector_width_int)
    print('Preferred vec width long', di.preferred_vector_width_long)
    print('Preferred vec width float', di.preferred_vector_width_float)
    print('Preferred vec width double', di.preferred_vector_width_double)
    print('Max clock frequency', di.max_clock_frequency)
    print('Address bits', di.address_bits)
    print('Max read image args', di.max_read_image_args)
    print('Max write image args', di.max_write_image_args)
    print('Max samplers', di.max_samplers)
    print('Mem base addr align', di.mem_base_addr_align)
    print('Min data type align', di.min_data_type_align_size)
    print('Gloabal mem cacheline size', di.global_mem_cacheline_size)
    print('Max constant args', di.max_constant_args)
    print('Device type', di.device_type)
    print('Max work group size', di.max_work_group_size)
    print('Image 2d max width', di.image2d_max_width)
    print('Image 2d max height', di.image2d_max_height)
    print('Image 3d max width', di.image3d_max_width)
    print('Image 3d max height', di.image3d_max_height)
    print('Image 3d max depth', di.image3d_max_depth)
    print('Max parameter size', di.max_parameter_size)
    print('Profiling timer resolution', di.profiling_timer_resolution)
    print('Platform ID', di.platform_id)
    print('Max mem alloc size', di.max_mem_alloc_size)
    print('Global mem cache size', di.global_mem_cache_size)
    print('Global mem size', di.global_mem_size)
    print('Max constant buffer size', di.max_constant_buffer_size)
    print('Local mem size', di.local_mem_size)
    print('Device name', di.name)
    print('Vendor', di.vendor)
    print('Version', di.version)
    print('Driver version', di.driver_version)
    print('Profile', di.profile)
    print('Extensions', di.extensions)

n = get_platform_ids()
platform_id = n[0]

print('Platform ID', platform_id)
display_pl_info(platform_id)
print('--------------------')

device_ids = get_default_device_ids(platform_id)
device_id = device_ids[0]
print(device_id)
display_device_info(device_id)

context = CLContext(platform_id, device_ids)
print(context.context)
