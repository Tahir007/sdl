
from random import random
import unittest
from tdasm import Runtime
from sdl.vector import Vector2, Vector3, Vector4
from sdl.shader import Shader
from sdl.args import IntArg, FloatArg, Vec2Arg, Vec3Arg, Vec4Arg


def ray_triangle_isect(v0, v1, v2, origin, direction): #ray direction must be normalized
        
        a = v0.x - v1.x
        b = v0.x - v2.x
        c = direction.x 
        d = v0.x - origin.x
        e = v0.y - v1.y
        f = v0.y - v2.y
        g = direction.y
        h = v0.y - origin.y
        i = v0.z - v1.z
        j = v0.z - v2.z
        k = direction.z
        l = v0.z - origin.z

        m = f * k - g * j
        n = h * k - g * l
        p = f * l - h * j
        q = g * i - e * k
        s = e * j - f * i

        temp3 =  (a * m + b * q + c * s)

        if temp3 == 0.0: return False
        inv_denom = 1.0 / temp3

        e1 = d * m - b * n - c * p
        beta = e1 * inv_denom

        if beta < 0.0: return False

        r = e * l - h * i
        e2 = a * n + d * q + c * r
        gamma = e2 * inv_denom

        if gamma < 0.0: return False

        if beta + gamma > 1.0: return False

        e3 = a * p - b * r + d * s
        t = e3 * inv_denom

        if t < 0.0: return False # self-intersection

        return (beta, gamma, t)


def calc_k(vec):
    x = abs(vec.x)
    y = abs(vec.y)
    z = abs(vec.z)
    if z > x and z > y:
        kz = 2
    elif y > x:
        kz = 1
    else:
        kz = 0
    kx = kz + 1
    if kx == 3:
        kx = 0
    ky = kx + 1
    if ky == 3:
        ky = 0

    d = {0:'x', 1:'y', 2:'z'}
    if getattr(vec, d[kz]) < 0.0:
        kx, ky = ky, kx
    sx = getattr(vec, d[kx]) / getattr(vec, d[kz]) 
    sy = getattr(vec, d[ky]) / getattr(vec, d[kz]) 
    sz = 1.0 / getattr(vec, d[kz]) 
    return (kx, ky, kz), (sx, sy, sz)


def ray_triangle(origin, direction, v1, v2, v3, k, s):
    kx, ky, kz = k
    sx, sy, sz = s
    A = v1 - origin
    B = v2 - origin
    C = v3 - origin
    d = {0:'x', 1:'y', 2:'z'}
    Ax = getattr(A, d[kx]) - sx * getattr(A, d[kz]) 
    Bx = getattr(B, d[kx]) - sx * getattr(B, d[kz]) 
    Cx = getattr(C, d[kx]) - sx * getattr(C, d[kz]) 
    print('ABCx', Ax, Bx, Cx)
    Ay = getattr(A, d[ky]) - sy * getattr(A, d[kz]) 
    By = getattr(B, d[ky]) - sy * getattr(B, d[kz]) 
    Cy = getattr(C, d[ky]) - sy * getattr(C, d[kz]) 
    print('ABCy', Ay, By, Cy)
    U = Cx * By - Cy * Bx
    V = Ax * Cy - Ay * Cx
    W = Bx * Ay - By * Ax
    print('UVW', U, V, W)
    Az = sz * getattr(A, d[kz]) 
    Bz = sz * getattr(B, d[kz]) 
    Cz = sz * getattr(C, d[kz]) 
    T = U * Az + V * Bz + W * Cz
    print('T,Det', T, U + V + W)
    rcp_det = 1.0 / (U + V + W)
    print('UVWT', rcp_det * U, rcp_det * V, rcp_det * W, rcp_det * T)


def generate_isect_ray_triangle():
    v1 = Vector3(3.64479, 19.4901, 31.67)
    v2 = Vector3(1.5157999, 0.663999, 31.8798)
    v3 = Vector3(1.4469, 0.663999, 31.415)
    origin = Vector3(280.0, 110.0, 244.0)
    direction = Vector3(-0.759278, -0.29809, -0.57847)
    direction.normalize()
    return origin, direction, v1, v2, v3 


def generate_problematic_isect_ray_triangle():
    v1 = Vector3(2.2, 4.4, 6.6)
    v2 = Vector3(1.1, 1.1, 1.1)
    v3 = Vector3(5.1, -1.1, 5.1)

    origin = Vector3(0.0, 0.0, 0.0)
    direction = Vector3(3.0, 3.0, 3.02) #3.00 -- precission problem investigate 
    direction.normalize()
    return origin, direction, v1, v2, v3 

def generate_problematic_isect_ray_triangle2():
    #backface culling
    v1 = Vector3(0.747954, 0.178927, 0.0192366)
    v2 = Vector3(0.647982, 0.883706, 0.0668817)
    v3 = Vector3(0.329859, 0.804180, 0.9238108)

    origin = Vector3(0.252753, 0.69336, 0.67819288)
    direction = Vector3(0.7554424, 0.477, 0.4491967)
    return origin, direction, v1, v2, v3 

def generate_problematic_isect_ray_triangle3():
    v1 = Vector3(0.01588845, 0.36193359, 0.248984399)
    v2 = Vector3(0.0965687, 0.4248549, 0.27277)
    v3 = Vector3(0.4137716, 0.587771, 0.591858)
    origin = Vector3(0.622582, 0.21039188, 0.52478)
    direction = Vector3(0.2407587, 0.173526, 0.954947)
    return origin, direction, v1, v2, v3 

def generate_random_ray_triangle():
    v1 = Vector3(random(), random(), random())
    v2 = Vector3(random(), random(), random())
    v3 = Vector3(random(), random(), random())

    origin = Vector3(random(), random(), random())
    direction = Vector3(random(), random(), random())
    direction.normalize()
    return origin, direction, v1, v2, v3 


class RayTriangleTests(unittest.TestCase):

    def test_precompute_watertight(self):
        code = """
ray = Ray()
ray.direction = vec
ray._kz = 55
ray._kx = 55
ray._ky = 55
precompute_ray_watertight(ray)
p1 = ray._kx
p2 = ray._ky
p3 = ray._kz
p4 = ray._sx
p5 = ray._sy
p6 = ray._sz

        """
        p1 = IntArg('p1', 333)
        p2 = IntArg('p2', 333)
        p3 = IntArg('p3', 333)
        p4 = FloatArg('p4', 22.2)
        p5 = FloatArg('p5', 22.2)
        p6 = FloatArg('p6', 22.2)
        vp = Vec3Arg('vec', Vector3(1.0, 1.0, 1.0))
        shader = Shader(code=code, args=[p1, p2, p3, p4, p5, p6, vp])
        shader.compile()
        shader.prepare(Runtime())

        vec = Vector3(0.2, 0.3, -0.8)
        vec.normalize()

        shader.set_value('vec', vec)
        shader.execute()
        v1 = shader.get_value('p1')
        v2 = shader.get_value('p2')
        v3 = shader.get_value('p3')
        v4 = shader.get_value('p4')
        v5 = shader.get_value('p5')
        v6 = shader.get_value('p6')
        k, s = calc_k(vec)

        kx, ky, kz = k
        self.assertEqual(kx*4, v1)
        self.assertEqual(ky*4, v2)
        self.assertEqual(kz*4, v3)

        sx, sy, sz = s

        self.assertAlmostEqual(sx, v4)
        self.assertAlmostEqual(sy, v5)
        self.assertAlmostEqual(sz, v6)

    def _create_ray_triangle_shader(self, isect_bool=False):
        code = """
ray = Ray()
ray.origin = origin
ray.direction = direction
precompute_ray_watertight(ray)
        """
        if isect_bool:
            code += """
ret = ray_triangle_watertight_b(ray, p1, p2, p3, min_dist)

            """
        else:
            code += """
ret = ray_triangle_watertight(ray, p1, p2, p3, min_dist)

            """
        p1 = Vec3Arg('p1', Vector3(0.0, 0.6, 0.3))
        p2 = Vec3Arg('p2', Vector3(0.5, 0.2, 0.9))
        p3 = Vec3Arg('p3', Vector3(0.2, 0.6, -0.3))
        mdist = FloatArg('min_dist', 555555.55)
        dire = Vec3Arg('direction', Vector3(0.5, 0.6, 0.3))
        orig = Vec3Arg('origin', Vector3(1.0, 1.0, 1.0))
        if isect_bool:
            ret = IntArg('ret', 44)
        else:
            ret = Vec4Arg('ret', Vector4(1.0, 1.0, 1.0, 1.0))
        shader = Shader(code=code, args=[p1, p2, p3, mdist, dire, orig, ret])
        shader.compile()
        shader.prepare(Runtime())
        return shader

    def test_ray_triangle_watertight(self):
        shader = self._create_ray_triangle_shader()

        origin, direction, v1, v2, v3 = generate_isect_ray_triangle()
        self._test_isect_ray_triangle(shader, v1, v2, v3, origin, direction)
        origin, direction, v1, v2, v3 = generate_problematic_isect_ray_triangle()
        self._test_isect_ray_triangle(shader, v1, v2, v3, origin, direction)

        origin, direction, v1, v2, v3 = generate_problematic_isect_ray_triangle2()
        self._test_isect_ray_triangle(shader, v1, v2, v3, origin, direction)

        origin, direction, v1, v2, v3 = generate_problematic_isect_ray_triangle3()
        self._test_isect_ray_triangle(shader, v1, v2, v3, origin, direction)

        nisects = 0
        for i in range(2000):
            origin, direction, v1, v2, v3 = generate_random_ray_triangle()
            isect_ocur = self._test_isect_ray_triangle(shader, v1, v2, v3, origin, direction)
            if isect_ocur:
                nisects += 1
        #print("Number of intersection ocur", nisects)

        #k, s = calc_k(direction)

        #shader.set_value('origin', origin)
        #shader.set_value('direction', direction)
        #shader.set_value('p1', v1)
        #shader.set_value('p2', v2)
        #shader.set_value('p3', v3)

        #shader.execute()
        #val = shader.get_value('ret')
        #print(val)
        #ray_triangle(origin, direction, v1, v2, v3, k, s)
        #beta, gamma, tt = ray_triangle_isect(v1, v2, v3, origin, direction)
        #print('BGT', beta, gamma, tt)

    def _test_isect_ray_triangle(self, shader, v1, v2, v3, origin, direction):

        shader.set_value('origin', origin)
        shader.set_value('direction', direction)
        shader.set_value('p1', v1)
        shader.set_value('p2', v2)
        shader.set_value('p3', v3)
        shader.execute()
        isect_ocur = True 
        # ret = UVWT
        ret = shader.get_value('ret')
        if ret.x < 0.0:
            isect_ocur = False
        u = ret.x
        v = ret.y
        w = ret.z
        t = ret.w

        beta, gamma, dist = 0.0, 0.0, 0.0
        isect_ocur2 = True 
        ret2 = ray_triangle_isect(v1, v2, v3, origin, direction)
        if ret2 is False:
            isect_ocur2 = False
        else:
            beta, gamma, dist = ret2

        if isect_ocur != isect_ocur2:
            print(ret)
            print(ret2)
            print(isect_ocur, isect_ocur2)
            print(v1)
            print(v2)
            print(v3)
            print(origin)
            print(direction)

        self.assertEqual(isect_ocur, isect_ocur2)
        if isect_ocur is True:
            self.assertAlmostEqual(v, beta, places=4)
            self.assertAlmostEqual(w, gamma, places=4)
            self.assertAlmostEqual(t, dist, places=3)
        return isect_ocur

    def test_ray_triangle_watertight_b(self):
        shader = self._create_ray_triangle_shader(isect_bool=True)
        nisects = 0
        for i in range(1000):
            origin, direction, v1, v2, v3 = generate_random_ray_triangle()
            shader.set_value('origin', origin)
            shader.set_value('direction', direction)
            shader.set_value('p1', v1)
            shader.set_value('p2', v2)
            shader.set_value('p3', v3)
            shader.execute()
            ret = shader.get_value('ret')
            isect_ocur = bool(ret)
            ret2 = ray_triangle_isect(v1, v2, v3, origin, direction)
            isect_ocur2 = bool(ret2)
            self.assertEqual(isect_ocur, isect_ocur2)

            if isect_ocur:
                nisects += 1

        #print("Number of intersection_b ocur", nisects)

if __name__ == "__main__":
    unittest.main()
