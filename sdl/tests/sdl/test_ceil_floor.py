
import unittest
from math import ceil, floor
from tdasm import Runtime
from sdl.vector import Vector3
from sdl.shader import Shader
from sdl.args import FloatArg, Vec3Arg, IntArg


class CeilFloorTests(unittest.TestCase):
    def test_ceil_fun(self):
        code = """
p1 = ceil(85)
p2 = ceil(-3.66)
p3 = ceil(2.0)
p4 = ceil(2.1)

        """
        p1 = IntArg('p1', 0)
        p2 = IntArg('p2', 0)
        p3 = IntArg('p3', 0)
        p4 = IntArg('p4', 0)

        shader = Shader(code=code, args=[p1, p2, p3, p4])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()

        p1 = shader.get_value('p1')
        self.assertEqual(p1, 85)
        p2 = shader.get_value('p2')
        self.assertEqual(p2, ceil(-3.66))
        p3 = shader.get_value('p3')
        self.assertEqual(p3, ceil(2.0))
        p4 = shader.get_value('p4')
        self.assertEqual(p4, ceil(2.1))

    def test_floor_fun(self):
        code = """
p1 = floor(85)
p2 = floor(-3.66)
p3 = floor(2.0)
p4 = floor(2.1)
p5 = floor(-5.0)

        """
        p1 = IntArg('p1', 0)
        p2 = IntArg('p2', 0)
        p3 = IntArg('p3', 0)
        p4 = IntArg('p4', 0)
        p5 = IntArg('p5', 0)

        shader = Shader(code=code, args=[p1, p2, p3, p4, p5])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()

        p1 = shader.get_value('p1')
        self.assertEqual(p1, 85)
        p2 = shader.get_value('p2')
        self.assertEqual(p2, floor(-3.66))
        p3 = shader.get_value('p3')
        self.assertEqual(p3, floor(2.0))
        p4 = shader.get_value('p4')
        self.assertEqual(p4, floor(2.1))
        p5 = shader.get_value('p5')
        self.assertEqual(p5, floor(-5.0))


if __name__ == "__main__":
    unittest.main()
