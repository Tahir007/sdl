
import unittest
from tdasm import Runtime
from sdl.shader import Shader
from sdl.args import FloatArg, register_struct, IntArg
from sdl.arr import ObjArray, ArrayArg, IntArray, FloatArray, ArrayArg


class ArrayBuiltInTests(unittest.TestCase):
    def test_built_in_int_array(self):
        code = """
tmp_array = array(100, int)
v = 55
tmp_array[2] = v
p1 = tmp_array[2] * 2

        """
        
        p1 = IntArg('p1', 0)
        shader = Shader(code=code, args=[p1])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()
        val = shader.get_value('p1')
        self.assertEqual(110, val)

    def test_built_in_float_array(self):
        code = """
tmp_array = array(100, float)
v = 0.21
tmp_array[55] = v
p1 = tmp_array[55]

        """
        
        p1 = FloatArg('p1', 0.0)
        shader = Shader(code=code, args=[p1])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()
        val = shader.get_value('p1')
        self.assertAlmostEqual(0.21, val)


    def test_built_in_obj_array(self):

        class SuperPoint:
            def __init__(self, x, y, end):
                self.x = x
                self.y = y
                self.end = end

        register_struct(SuperPoint, 'SuperPoint', fields=[
            ('x', FloatArg),
            ('y', FloatArg),
            ('end', IntArg),
            ],
            factory=lambda: SuperPoint(1.0, 2.0, 22))

        code = """
tmp_array = array(50, SuperPoint)
o = tmp_array[3]
o.x = 3.3
o.y = 2.2
o.end = 34
o3 = tmp_array[3]
p1 = o3.x 
p2 = o3.y
p3 = o3.end

end = 222

        """

        p1 = FloatArg('p1', 0.0)
        p2 = FloatArg('p2', 0.0)
        p3 = IntArg('p3', 0)
        p4 = IntArg('end', 0)
        shader = Shader(code=code, args=[p1, p2, p3, p4])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()

        val = shader.get_value('p1')
        self.assertAlmostEqual(3.3, val)
        val = shader.get_value('p2')
        self.assertAlmostEqual(2.2, val)
        val = shader.get_value('p3')
        self.assertEqual(34, val)
        val = shader.get_value('end')
        self.assertEqual(222, val)


if __name__ == "__main__":
    unittest.main()
