
import unittest
from tdasm import Runtime
from sdl.vector import Vector3, Vector2
from sdl.shader import Shader
from sdl.args import FloatArg, Vec3Arg, IntArg, Vec2Arg


def ray_bbox_isect(origin, direction, bbox_min, bbox_max):
    ox = origin.x
    oy = origin.y
    oz = origin.z
    dx = direction.x
    dy = direction.y
    dz = direction.z
    x0 = bbox_min.x
    x1 = bbox_max.x
    y0 = bbox_min.y
    y1 = bbox_max.y
    z0 = bbox_min.z
    z1 = bbox_max.z

    a = 1.0 / dx
    if a >= 0:
        tx_min = (x0 - ox) * a
        tx_max = (x1 - ox) * a
    else:
        tx_min = (x1 - ox) * a
        tx_max = (x0 - ox) * a

    b = 1.0 / dy
    if b >= 0:
        ty_min = (y0 - oy) * b
        ty_max = (y1 - oy) * b
    else:
        ty_min = (y1 - oy) * b
        ty_max = (y0 - oy) * b

    c = 1.0 / dz
    if c >= 0:
        tz_min = (z0 - oz) * c
        tz_max = (z1 - oz) * c
    else:
        tz_min = (z1 - oz) * c
        tz_max = (z0 - oz) * c

    print(tx_min, ty_min, tz_min)
    print(tx_max, ty_max, tz_max)

    if tx_min > ty_min:
        t0 = tx_min
    else:
        t0 = ty_min

    if tz_min > t0:
        t0 = tz_min

    if tx_max < ty_max:
        t1 = tx_max
    else:
        t1 = ty_max

    if tz_max < t1:
        t1 = tz_max

    print(t0, t1)

    if t0 < t1 and t1 > 0.00001:
        return True
    else:
        return False


class RayBBoxTests(unittest.TestCase):
    def test_ray_bbox_isect(self):
        code = """

# arguments, inv_direction, origin, bbox_min, bbox_max
inv_x = 1.0 / direction[0]
inv_y = 1.0 / direction[1]
inv_z = 1.0 / direction[2]
inv_dir = float3(inv_x, inv_y, inv_z)

result = ray_bbox_isect(inv_dir, origin, bbox_min, bbox_max)

        """

        origin = Vector3(0.0, 0.0, 0.0)
        direction = Vector3(2.0, 1.8, 2.1)
        direction.normalize()
        bbox_min = Vector3(1.0, 1.0, 1.0)
        bbox_max = Vector3(5.0, 5.0, 5.0)

        result = ray_bbox_isect(origin, direction, bbox_min, bbox_max)

        p1 = Vec3Arg('origin', origin)
        p2 = Vec3Arg('direction', direction)
        p3 = Vec3Arg('bbox_min', bbox_min)
        p4 = Vec3Arg('bbox_max', bbox_max)
        p5 = Vec2Arg('result', Vector2(0.0, 0.0))

        shader = Shader(code=code, args=[p1, p2, p3, p4, p5])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()
        print(shader.get_value('result'))

if __name__ == "__main__":
    unittest.main()
