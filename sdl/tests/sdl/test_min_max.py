
import unittest
from tdasm import Runtime
from sdl.vector import Vector3
from sdl.shader import Shader
from sdl.args import FloatArg, Vec3Arg, IntArg


class MinMaxTests(unittest.TestCase):
    def test_min_fun(self):
        code = """
p1 = min(41, 22)
p2 = min(3.66, 2.2)
p3 = min((-2.5, 3.6, -1.2), (5.5, 1.1, 3.3))
p4 = min(11, -66)
        """
        p1 = IntArg('p1', 0)
        p2 = FloatArg('p2', 0.0)
        p3 = Vec3Arg('p3', Vector3(0.0, 0.0, 0.0))
        p4 = IntArg('p4', 0)

        shader = Shader(code=code, args=[p1, p2, p3, p4])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()

        p1 = shader.get_value('p1')
        self.assertEqual(p1, min(41, 22))
        p2 = shader.get_value('p2')
        self.assertAlmostEqual(p2, min(3.66, 2.2), places=6)
        p3 = shader.get_value('p3')
        self.assertAlmostEqual(p3.x, min(-2.5, 5.5), places=6)
        self.assertAlmostEqual(p3.y, min(3.6, 1.1), places=6)
        self.assertAlmostEqual(p3.z, min(-1.2, 3.3), places=6)
        p4 = shader.get_value('p4')
        self.assertEqual(p4, min(11, -66))

    def test_max_fun(self):
        code = """
p1 = max(41, 22)
p2 = max(3.66, 2.2)
p3 = max((-2.5, 3.6, -1.2), (5.5, 1.1, 3.3))
p4 = max(-55, 33)
        """
        p1 = IntArg('p1', 0)
        p2 = FloatArg('p2', 0.0)
        p3 = Vec3Arg('p3', Vector3(0.0, 0.0, 0.0))
        p4 = IntArg('p4', 0)

        shader = Shader(code=code, args=[p1, p2, p3, p4])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()

        p1 = shader.get_value('p1')
        self.assertEqual(p1, max(41, 22))
        p2 = shader.get_value('p2')
        self.assertAlmostEqual(p2, max(3.66, 2.2), places=6)
        p3 = shader.get_value('p3')
        self.assertAlmostEqual(p3.x, max(-2.5, 5.5), places=6)
        self.assertAlmostEqual(p3.y, max(3.6, 1.1), places=6)
        self.assertAlmostEqual(p3.z, max(-1.2, 3.3), places=6)
        p4 = shader.get_value('p4')
        self.assertEqual(p4, max(-55, 33))

if __name__ == "__main__":
    unittest.main()
