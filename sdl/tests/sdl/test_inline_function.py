
import unittest
from tdasm import Runtime
from sdl.vector import Vector3
from sdl.shader import Shader
from sdl.args import FloatArg, Vec3Arg, IntArg


class InlineFunction(unittest.TestCase):

    def test_inline_function(self):
        code = """
def square(x):
    return x * x

def smart_func(x):

    def qube(y):
        return x + y + 2.0
    
    y = x * 2 
    return qube(y)
        

x = 6 
p1 = square(x)

y = 7.0
p2 = square(y)

z = float3(3.0, 4.0, 5.0)
p3 = square(z)

p4 = smart_func(4.0)

        """
        p1 = IntArg('p1', 0)
        p2 = FloatArg('p2', 0.0)
        p3 = Vec3Arg('p3', Vector3(0.0, 0.0, 0.0))
        p4 = FloatArg('p4', 0.0)
        shader = Shader(code=code, args=[p1, p2, p3, p4])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()
        #print(shader._asm_codes[0])

        p1 = shader.get_value('p1')
        self.assertEqual(p1, 36)
        p2 = shader.get_value('p2')
        self.assertAlmostEqual(p2, 49.0)
        p3 = shader.get_value('p3')
        self.assertAlmostEqual(p3.x, 9.0)
        self.assertAlmostEqual(p3.y, 16.0)
        self.assertAlmostEqual(p3.z, 25.0)
        p4 = shader.get_value('p4')
        self.assertAlmostEqual(p4, 14.0)

if __name__ == "__main__":
    unittest.main()
