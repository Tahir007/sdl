
import unittest
from tdasm import Runtime
from sdl.shader import Shader
from sdl.args import FloatArg, register_struct, IntArg
from sdl.arr import ObjArray, ArrayArg, IntArray, FloatArray


class SuperPoint:
    def __init__(self, x, y):
        self.x = x
        self.y = y

register_struct(SuperPoint, 'SuperPoint',
                fields=[('x', FloatArg), ('y', FloatArg)],
                factory=lambda: SuperPoint(1.0, 2.0))


class ArrayTests(unittest.TestCase):
    def test_object_array(self):

        p = SuperPoint(3.0, 4.0)
        arr = ObjArray(p)
        arr.append(p)
        arr.append(p)
        p = SuperPoint(7.0, 9.0)
        arr.append(p)

        code = """
index = 2
temp = arr[index]
p1 = temp.x
temp.y = 12.34
        """
        arg = ArrayArg('arr', arr)
        arg1 = FloatArg('p1', 4.4)
        shader = Shader(code=code, args=[arg, arg1])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()
        val = shader.get_value('p1')
        self.assertAlmostEqual(val, 7.0)
        obj = arr[2]
        self.assertAlmostEqual(obj.y, 12.34, 6)

    def test_int_array(self):
        arr = IntArray()
        arr.append(5)
        arr.append(7)
        arg = ArrayArg('arr', arr)

        code = """
index = 1
val = arr[index]
gg = 55
index = 0
arr[index] = gg
        """
        varg = IntArg('val', 0)
        shader = Shader(code=code, args=[arg, varg])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()
        self.assertEqual(7, shader.get_value('val'))
        self.assertEqual(55, arr[0])

    def test_float_array(self):
        arr = FloatArray()
        arr.append(5.5)
        arr.append(7.7)
        arg = ArrayArg('arr', arr)

        code = """
index = 1
val = arr[index]
br = 3.3
arr[0] = br
        """
        varg = FloatArg('val', 0.0)
        shader = Shader(code=code, args=[arg, varg])
        shader.compile()
        shader.prepare(Runtime())
        shader.execute()
        self.assertAlmostEqual(7.7, shader.get_value('val'), 6)
        self.assertAlmostEqual(3.3, arr[0], 6)


if __name__ == "__main__":
    unittest.main()
