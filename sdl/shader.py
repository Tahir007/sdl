"""
    Implementation of Shader that is used for compiling and
    exectuing shader.
"""

import x86
from tdasm import Tdasm
from .parser import parse
from .cgen import CodeGenerator
from .args import ArgList
from .ext_func import load_ext_function


class Shader:
    def __init__(self, code, args=[], is_func=False,
                 name=None, func_args=[]):

        self._code = code
        self._args = args
        self._is_func = is_func
        if is_func and name is None:
            raise ValueError("Function shader must have name!")

        self._name = name
        self._func_args = func_args

        self._ds = None
        self._args_map = {}
        for arg in self._args:
            self._args_map[arg.name] = arg

        self._ret_type = None
        self._mcs = None

    @property
    def ret_type(self):
        return self._ret_type

    @property
    def name(self):
        return self._name

    @property
    def func_args(self):
        return self._func_args

    @property
    def args(self):
        return self._args

    def _recode_asm(self, shaders, asm_code):

        def nmcs(shader):
            if shader._mcs is None:
                name = shader.name if shader.name is not None else ''
                raise ValueError("Shader %s is not yet compiled!" % name)
            return len(shader._mcs)

        def get_nthreads(shader):
            n = 1
            for arg in shader.args:
                if isinstance(arg, ArgList):
                    n = len(arg.args)
            return n

        def replace_label(shader, index, code):
            old_name = shader.name
            new_name = '%s%i' % (shader.name, index)
            return code.replace(old_name, new_name)

        nthreads = 1
        for shader in shaders:
            nthreads = max(nthreads, nmcs(shader))
        nthreads = max(nthreads, get_nthreads(self))

        asm_codes = [str(asm_code) for i in range(nthreads)]
        if nthreads > 1:
            for shader in shaders:
                if shader.name is not None and nmcs(shader) > 1:
                    for n in range(nthreads):
                        asm_codes[n] = replace_label(shader, n, asm_codes[n])
            if self.name is not None:
                for n in range(nthreads):
                    asm_codes[n] = replace_label(self, n, asm_codes[n])
        return asm_codes

    def compile(self, shaders=[], color_mgr=None):
        stms = parse(self._code)
        cgen = CodeGenerator()
        asm, ret_type, fns = cgen.generate_code(stms, args=self._args,
                                                is_func=self._is_func,
                                                name=self._name,
                                                func_args=self._func_args,
                                                shaders=shaders,
                                                color_mgr=color_mgr)

        self._asm_codes = self._recode_asm(shaders, asm)
        self._ret_type = ret_type
        self._ext_functions = fns
        asm = Tdasm()
        self._mcs = [asm.assemble(code, naked=self._is_func, ia32=not cgen.BIT64)
                     for code in self._asm_codes]

    def prepare(self, runtime):
        for fn in self._ext_functions.values():
            load_ext_function(runtime, fn)

        names = ['shader_%i_%i' % (id(self), i) for i in range(len(self._mcs))]
        self._ds = [runtime.load(name, mc) for name, mc in zip(names, self._mcs)]
        self._runtime = runtime
        self._addrs = tuple(runtime.address_module(name) for name in names)

        self.update_args()

    def get_ptrs(self):
        if self.name is None:
            raise ValueError("Only shader that have name can have function pointers!")
        runtime = self._runtime
        if len(self._mcs) == 1:
            ptrs = [runtime.address_label(self.name)]
        else:
            ptrs = [runtime.address_label('%s%i' % (self.name, i)) for i, mc in enumerate(self._mcs)]
        return ptrs

    def update_args(self):
        if self._ds is None:
            return
        for arg in self._args:
            self._update_arg(arg)

    def _update_arg(self, arg, index=None):
        if isinstance(arg, ArgList):
            arg.update(self._ds, index)
        else:
            for ds in self._ds:
                arg.update(ds)

    def _get_arg(self, name):
        return self._args_map[name]

    def set_value(self, name, value, index=None):
        arg = self._get_arg(name)
        if isinstance(arg, ArgList) and index is not None:
            arg.set_value(value, index)
            if self._ds is not None:
                self._update_arg(arg, index)
            return

        arg.value = value
        if self._ds is not None:
            self._update_arg(arg)

    def get_value(self, name):
        arg = self._get_arg(name)
        if self._ds is None:
            return arg.value
        if isinstance(arg, ArgList):
            return arg.from_ds(self._ds)
        else:
            return arg.from_ds(self._ds[0])

    def execute(self):
        if self._is_func:
            raise ValueError("Function shader cannot be directly executed.")

        if len(self._mcs) == 1:
            name = 'shader_%i_0' % id(self)
            self._runtime.run(name)
        else:
            x86.ExecuteModules(self._addrs)

    def run_threads(self, thread_index=-1):
        if self._is_func:
            raise ValueError("Function shader cannot be directly executed.")

        if thread_index == -1:
            ret_addr = x86.RunThreads(self._addrs)
        else:
            ret_addr = x86.RunThreads(self._addrs[thread_index])
        return self._addrs.index(ret_addr)

    def wait_threads(self):
        x86.RunThreads(0)
