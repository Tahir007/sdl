
import platform
import ctypes
from ctypes import byref, c_void_p, c_uint32, c_uint64, create_string_buffer,\
    sizeof, c_size_t, c_ulong, c_ulonglong, c_char, c_int32
from ctypes.util import find_library


CL_SUCCESS =                                  0
CL_DEVICE_NOT_FOUND =                         -1
CL_DEVICE_NOT_AVAILABLE =                     -2
CL_COMPILER_NOT_AVAILABLE =                   -3
CL_MEM_OBJECT_ALLOCATION_FAILURE =            -4
CL_OUT_OF_RESOURCES =                         -5
CL_OUT_OF_HOST_MEMORY =                       -6
CL_PROFILING_INFO_NOT_AVAILABLE =             -7
CL_MEM_COPY_OVERLAP =                         -8
CL_IMAGE_FORMAT_MISMATCH =                    -9
CL_IMAGE_FORMAT_NOT_SUPPORTED =               -10
CL_BUILD_PROGRAM_FAILURE =                    -11
CL_MAP_FAILURE =                              -12
CL_MISALIGNED_SUB_BUFFER_OFFSET =             -13
CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST = -14
CL_INVALID_VALUE =                            -30
CL_INVALID_DEVICE_TYPE =                      -31
CL_INVALID_PLATFORM =                         -32
CL_INVALID_DEVICE =                           -33
CL_INVALID_CONTEXT =                          -34
CL_INVALID_QUEUE_PROPERTIES =                 -35
CL_INVALID_COMMAND_QUEUE =                    -36
CL_INVALID_HOST_PTR =                         -37
CL_INVALID_MEM_OBJECT =                       -38
CL_INVALID_IMAGE_FORMAT_DESCRIPTOR =          -39
CL_INVALID_IMAGE_SIZE =                       -40
CL_INVALID_SAMPLER =                          -41
CL_INVALID_BINARY =                           -42
CL_INVALID_BUILD_OPTIONS =                    -43
CL_INVALID_PROGRAM =                          -44
CL_INVALID_PROGRAM_EXECUTABLE =               -45
CL_INVALID_KERNEL_NAME =                      -46
CL_INVALID_KERNEL_DEFINITION =                -47
CL_INVALID_KERNEL =                           -48
CL_INVALID_ARG_INDEX =                        -49
CL_INVALID_ARG_VALUE =                        -50
CL_INVALID_ARG_SIZE =                         -51
CL_INVALID_KERNEL_ARGS =                      -52
CL_INVALID_WORK_DIMENSION =                   -53
CL_INVALID_WORK_GROUP_SIZE =                  -54
CL_INVALID_WORK_ITEM_SIZE =                   -55
CL_INVALID_GLOBAL_OFFSET =                    -56
CL_INVALID_EVENT_WAIT_LIST =                  -57
CL_INVALID_EVENT =                            -58
CL_INVALID_OPERATION =                        -59
CL_INVALID_GL_OBJECT =                        -60
CL_INVALID_BUFFER_SIZE =                      -61
CL_INVALID_MIP_LEVEL =                        -62
CL_INVALID_GLOBAL_WORK_SIZE =                 -63
CL_INVALID_PROPERTY =                         -64


CL_PLATFORM_PROFILE =                         0x0900
CL_PLATFORM_VERSION =                         0x0901
CL_PLATFORM_NAME =                            0x0902
CL_PLATFORM_VENDOR =                          0x0903
CL_PLATFORM_EXTENSIONS =                      0x0904


CL_DEVICE_TYPE_DEFAULT =                      (1 << 0)
CL_DEVICE_TYPE_CPU =                          (1 << 1)
CL_DEVICE_TYPE_GPU =                          (1 << 2)
CL_DEVICE_TYPE_ACCELERATOR =                  (1 << 3)
CL_DEVICE_TYPE_ALL =                          0xFFFFFFFF


CL_DEVICE_TYPE =                              0x1000
CL_DEVICE_VENDOR_ID =                         0x1001
CL_DEVICE_MAX_COMPUTE_UNITS =                 0x1002
CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS =          0x1003
CL_DEVICE_MAX_WORK_GROUP_SIZE =               0x1004
CL_DEVICE_MAX_WORK_ITEM_SIZES =               0x1005
CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR =       0x1006
CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT =      0x1007
CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT =        0x1008
CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG =       0x1009
CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT =      0x100A
CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE =     0x100B
CL_DEVICE_MAX_CLOCK_FREQUENCY =               0x100C
CL_DEVICE_ADDRESS_BITS =                      0x100D
CL_DEVICE_MAX_READ_IMAGE_ARGS =               0x100E
CL_DEVICE_MAX_WRITE_IMAGE_ARGS =              0x100F
CL_DEVICE_MAX_MEM_ALLOC_SIZE =                0x1010
CL_DEVICE_IMAGE2D_MAX_WIDTH =                 0x1011
CL_DEVICE_IMAGE2D_MAX_HEIGHT =                0x1012
CL_DEVICE_IMAGE3D_MAX_WIDTH =                 0x1013
CL_DEVICE_IMAGE3D_MAX_HEIGHT =                0x1014
CL_DEVICE_IMAGE3D_MAX_DEPTH =                 0x1015
CL_DEVICE_IMAGE_SUPPORT =                     0x1016
CL_DEVICE_MAX_PARAMETER_SIZE =                0x1017
CL_DEVICE_MAX_SAMPLERS =                      0x1018
CL_DEVICE_MEM_BASE_ADDR_ALIGN =               0x1019
CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE =          0x101A
CL_DEVICE_SINGLE_FP_CONFIG =                  0x101B
CL_DEVICE_GLOBAL_MEM_CACHE_TYPE =             0x101C
CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE =         0x101D
CL_DEVICE_GLOBAL_MEM_CACHE_SIZE =             0x101E
CL_DEVICE_GLOBAL_MEM_SIZE =                   0x101F
CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE =          0x1020
CL_DEVICE_MAX_CONSTANT_ARGS =                 0x1021
CL_DEVICE_LOCAL_MEM_TYPE =                    0x1022
CL_DEVICE_LOCAL_MEM_SIZE =                    0x1023
CL_DEVICE_ERROR_CORRECTION_SUPPORT =          0x1024
CL_DEVICE_PROFILING_TIMER_RESOLUTION =        0x1025
CL_DEVICE_ENDIAN_LITTLE =                     0x1026
CL_DEVICE_AVAILABLE =                         0x1027
CL_DEVICE_COMPILER_AVAILABLE =                0x1028
CL_DEVICE_EXECUTION_CAPABILITIES =            0x1029
CL_DEVICE_QUEUE_PROPERTIES =                  0x102A
CL_DEVICE_NAME =                              0x102B
CL_DEVICE_VENDOR =                            0x102C
CL_DRIVER_VERSION =                           0x102D
CL_DEVICE_PROFILE =                           0x102E
CL_DEVICE_VERSION =                           0x102F
CL_DEVICE_EXTENSIONS =                        0x1030
CL_DEVICE_PLATFORM =                          0x1031
CL_DEVICE_DOUBLE_FP_CONFIG =                  0x1032
CL_DEVICE_HALF_FP_CONFIG =                    0x1033
CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF =       0x1034
CL_DEVICE_HOST_UNIFIED_MEMORY =               0x1035
CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR =          0x1036
CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT =         0x1037
CL_DEVICE_NATIVE_VECTOR_WIDTH_INT =           0x1038
CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG =          0x1039
CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT =         0x103A
CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE =        0x103B
CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF =          0x103C
CL_DEVICE_OPENCL_C_VERSION =                  0x103D


CL_CONTEXT_REFERENCE_COUNT =                  0x1080
CL_CONTEXT_DEVICES =                          0x1081
CL_CONTEXT_PROPERTIES =                       0x1082
CL_CONTEXT_NUM_DEVICES =                      0x1083
CL_CONTEXT_PLATFORM =                         0x1084


def load_opencl_library():

    path = find_library('OpenCL')
    if path is None:
        raise ValueError('OpenCL library cannot be found')

    system = platform.system()
    if system == 'Linux':
        loader = ctypes.cdll
    elif system == 'Windows':
        loader = ctypes.windll
    else:
        raise ValueError('Unsuported operating system %s' % system)

    return loader.LoadLibrary(path)


_opencl_library = None

def get_opencl_library():
    global _opencl_library

    if _opencl_library is not None:
        return _opencl_library

    _opencl_library = load_opencl_library()
    return _opencl_library


def get_num_of_available_platforms():
    cl = get_opencl_library()
    clGetPlatformIDs = cl.clGetPlatformIDs
    num_platforms = c_uint32()
    ret = clGetPlatformIDs(0, None, byref(num_platforms))
    if ret != CL_SUCCESS:
        raise ValueError("clGetPlatformIDs not executed successfully! ErrCode = %i" % ret)
    return num_platforms.value


def get_platform_ids():
    cl = get_opencl_library()
    clGetPlatformIDs = cl.clGetPlatformIDs
    num_platforms = get_num_of_available_platforms()

    platform_array = (c_void_p * num_platforms)()
    ret = clGetPlatformIDs(num_platforms, platform_array, None)
    if ret != CL_SUCCESS:
        raise ValueError("clGetPlatformIDs not executed successfully! ErrCode = %i" % ret)
    return tuple(platform_id for platform_id in platform_array) 


def get_platform_info(platform_id, param_name):
    cl = get_opencl_library()
    clGetPlatformInfo = cl.clGetPlatformInfo
    param_size = c_uint32()
    ret = clGetPlatformInfo(c_void_p(platform_id), param_name, 0, None, byref(param_size))
    if ret != CL_SUCCESS:
        raise ValueError("clGetPlatformInfo not executed successfully! ErrCode = %i" % ret)

    buff = create_string_buffer(param_size.value)
    ret = clGetPlatformInfo(c_void_p(platform_id), param_name, param_size.value, buff, None)
    if ret != CL_SUCCESS:
        raise ValueError("clGetPlatformInfo not executed successfully! ErrCode = %i" % ret)
    return str(buff.value, 'utf-8')


class DeviceNotFound(Exception):
    pass


def get_device_ids(platform_id, device_type=CL_DEVICE_TYPE_DEFAULT):
    cl = get_opencl_library()
    clGetDeviceIDs = cl.clGetDeviceIDs
    
    dev_type = c_uint64(device_type)
    num_devices = c_uint32()
    ret = clGetDeviceIDs(c_void_p(platform_id), dev_type, 0, None, byref(num_devices))
    if ret == CL_DEVICE_NOT_FOUND:
        raise DeviceNotFound("OpenCL device not found for platofrm %i" % platform_id)
    if ret != CL_SUCCESS:
        raise ValueError("clGetDeviceIDs not executed successfully! ErrCode = %i" % ret)
    print (num_devices.value)

    device_ids = (c_void_p * num_devices.value)()
    ret = clGetDeviceIDs(c_void_p(platform_id), dev_type, num_devices.value, device_ids, None)
    if ret != CL_SUCCESS:
        raise ValueError("clGetDeviceIDs not executed successfully! ErrCode = %i" % ret)
    return tuple(device_id for device_id in device_ids) 


def get_default_device_ids(platform_id):
    try:
        ids = get_device_ids(platform_id, CL_DEVICE_TYPE_GPU)
    except DeviceNotFound:
        ids = get_device_ids(platform_id, CL_DEVICE_TYPE_DEFAULT)
    return ids


def get_device_info(device_id, param_name, ret_type):

    cl = get_opencl_library()
    clGetDeviceInfo = cl.clGetDeviceInfo
    size = c_uint32()
    param_name = c_uint32(param_name)

    if ret_type in (c_uint32, c_uint64, c_size_t, c_ulong, c_void_p):
        n = ret_type()
        ret = clGetDeviceInfo(c_void_p(device_id), param_name, sizeof(ret_type), byref(n), byref(size))
        if ret != CL_SUCCESS:
            raise ValueError("clGetDeviceInfo not executed successfully! ErrCode = %i" % ret)
        return n.value
    elif ret_type is c_char:
        ret = clGetDeviceInfo(c_void_p(device_id), param_name, 0, None, byref(size))
        if ret != CL_SUCCESS:
            raise ValueError("clGetDeviceInfo not executed successfully! ErrCode = %i" % ret)

        buff = create_string_buffer(size.value)
        ret = clGetDeviceInfo(c_void_p(device_id), param_name, size.value, buff, None)
        if ret != CL_SUCCESS:
            raise ValueError("clGetDeviceInfo not executed successfully! ErrCode = %i" % ret)
        return str(buff.value, 'utf-8')


def create_context(platform_id, device_ids):
    cl = get_opencl_library()
    clCreateContext = cl.clCreateContext

    props = (c_void_p * 3)()
    props[0] = CL_CONTEXT_PLATFORM
    props[1] = c_void_p(platform_id)
    props[2] = 0

    dev_arr = (c_void_p * len(device_ids))()
    for i in range(len(device_ids)):
        dev_arr[i] = device_ids[i]

    err_code = c_int32()
    context = clCreateContext(props, len(device_ids), dev_arr, None, None, byref(err_code))
    if err_code.value != CL_SUCCESS:
        raise ValueError("clCreateContext not executed successfully! ErrCode = %i" % err_code.value)
    return context


class CLPlatformInfo:
    def __init__(self, platform_id):
        self.platform_id = platform_id

    @property
    def name(self):
        return get_platform_info(self.platform_id, CL_PLATFORM_NAME)

    @property
    def vendor(self):
        return get_platform_info(self.platform_id, CL_PLATFORM_VENDOR)

    @property
    def version(self):
        return get_platform_info(self.platform_id, CL_PLATFORM_VERSION)

    @property
    def extensions(self):
        return get_platform_info(self.platform_id, CL_PLATFORM_EXTENSIONS)

    @property
    def profile(self):
        return get_platform_info(self.platform_id, CL_PLATFORM_PROFILE)


class CLDeviceInfo:
    def __init__(self, device_id):
        self.device_id = device_id

    @property
    def vendor_id(self):
        return get_device_info(self.device_id, CL_DEVICE_VENDOR_ID, c_uint32)

    @property
    def max_compute_units(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_COMPUTE_UNITS, c_uint32)

    @property
    def max_work_item_dims(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, c_uint32)

    @property
    def preferred_vector_width_char(self):
        return get_device_info(self.device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, c_uint32)

    @property
    def preferred_vector_width_short(self):
        return get_device_info(self.device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, c_uint32)

    @property
    def preferred_vector_width_int(self):
        return get_device_info(self.device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, c_uint32)

    @property
    def preferred_vector_width_long(self):
        return get_device_info(self.device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, c_uint32)

    @property
    def preferred_vector_width_float(self):
        return get_device_info(self.device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, c_uint32)

    @property
    def preferred_vector_width_double(self):
        return get_device_info(self.device_id, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, c_uint32)

    @property
    def max_clock_frequency(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_CLOCK_FREQUENCY, c_uint32)

    @property
    def address_bits(self):
        return get_device_info(self.device_id, CL_DEVICE_ADDRESS_BITS, c_uint32)

    @property
    def max_read_image_args(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_READ_IMAGE_ARGS, c_uint32)

    @property
    def max_write_image_args(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, c_uint32)

    @property
    def max_samplers(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_SAMPLERS, c_uint32)

    @property
    def mem_base_addr_align(self):
        return get_device_info(self.device_id, CL_DEVICE_MEM_BASE_ADDR_ALIGN, c_uint32)

    @property
    def min_data_type_align_size(self):
        return get_device_info(self.device_id, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, c_uint32)

    @property
    def global_mem_cacheline_size(self):
        return get_device_info(self.device_id, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, c_uint32)

    @property
    def max_constant_args(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_CONSTANT_ARGS, c_uint32)

    @property
    def device_type(self):
        return get_device_info(self.device_id, CL_DEVICE_TYPE, c_uint64)

    @property
    def max_work_group_size(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_WORK_GROUP_SIZE, c_size_t)

    @property
    def image2d_max_width(self):
        return get_device_info(self.device_id, CL_DEVICE_IMAGE2D_MAX_WIDTH , c_size_t)

    @property
    def image2d_max_height(self):
        return get_device_info(self.device_id, CL_DEVICE_IMAGE2D_MAX_HEIGHT, c_size_t)

    @property
    def image3d_max_width(self):
        return get_device_info(self.device_id, CL_DEVICE_IMAGE3D_MAX_WIDTH , c_size_t)

    @property
    def image3d_max_height(self):
        return get_device_info(self.device_id, CL_DEVICE_IMAGE3D_MAX_HEIGHT, c_size_t)

    @property
    def image3d_max_depth(self):
        return get_device_info(self.device_id, CL_DEVICE_IMAGE3D_MAX_DEPTH, c_size_t)

    @property
    def max_parameter_size(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_PARAMETER_SIZE, c_size_t)

    @property
    def profiling_timer_resolution(self):
        return get_device_info(self.device_id, CL_DEVICE_PROFILING_TIMER_RESOLUTION, c_size_t)

    @property
    def platform_id(self):
        return get_device_info(self.device_id, CL_DEVICE_PLATFORM, c_void_p)

    @property
    def max_mem_alloc_size(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_MEM_ALLOC_SIZE, c_ulonglong)

    @property
    def global_mem_cache_size(self):
        return get_device_info(self.device_id, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, c_ulonglong)

    @property
    def global_mem_size(self):
        return get_device_info(self.device_id, CL_DEVICE_GLOBAL_MEM_SIZE, c_ulonglong)

    @property
    def max_constant_buffer_size(self):
        return get_device_info(self.device_id, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, c_ulonglong)

    @property
    def local_mem_size(self):
        return get_device_info(self.device_id, CL_DEVICE_LOCAL_MEM_SIZE, c_ulonglong)

    @property
    def name(self):
        return get_device_info(self.device_id, CL_DEVICE_NAME, c_char)

    @property
    def vendor(self):
        return get_device_info(self.device_id, CL_DEVICE_VENDOR, c_char)

    @property
    def version(self):
        return get_device_info(self.device_id, CL_DEVICE_VERSION, c_char)

    @property
    def driver_version(self):
        return get_device_info(self.device_id, CL_DRIVER_VERSION, c_char)

    @property
    def profile(self):
        return get_device_info(self.device_id, CL_DEVICE_PROFILE, c_char)

    @property
    def extensions(self):
        return get_device_info(self.device_id, CL_DEVICE_EXTENSIONS, c_char)


class CLContext:
    def __init__(self, platform_id, device_ids):
        self.platform_id = platform_id
        self.device_ids = device_ids
        self.context = create_context(platform_id, device_ids)

