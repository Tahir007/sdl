
import platform
from tdasm import Tdasm
import x86
from .dynamic_array import DynamicArray
from .args import arg_from_value, StructArg, _struct_desc, Argument, get_struct_desc
from .memcpy import memcpy


class Array:
    def __init__(self):
        pass


class StackObjArray(Array):
    def __init__(self, size, type_name):
        self._size = size
        self._type_name = type_name

        desc = get_struct_desc(type_name=type_name)
        value = desc.factory()
        self._arg = arg_from_value('dummy', value)

    def __len__(self):
        return self._size

    @property
    def item_size(self):
        desc = get_struct_desc(type_name=self._type_name)
        return desc.struct.sizeof()

    @property
    def item_arg(self):
        return self._arg

    # def address(self):
    #     return 0


class ObjArray(Array):
    def __init__(self, value):
        arg = arg_from_value('p1', value)
        if not isinstance(arg, StructArg):
            raise ValueError("Only struct argument in object array!", arg)
        struct = get_struct_desc(type_name=arg.type_name).struct
        self._dyn_arr = DynamicArray(struct)
        
        self._arg = arg
        self._objs = []

    def append(self, obj):
        typ = type(obj)
        if not isinstance(self._arg.value, typ):
            raise ValueError("Uncompatable types for array", obj)
        desc = _struct_desc[type(self._arg.value)]
        values = {}
        # TODO struct inside struct
        for name, arg_type in desc.fields:
            val = getattr(obj, name)
            values[name] = arg_type.conv_to_ds(val)
        self._dyn_arr.add_instance(values)
        self._objs.append(obj)

    def update(self, obj):
        desc = _struct_desc[type(self._arg.value)]
        values = {}
        for name, arg_type in desc.fields:
            val = getattr(obj, name)
            values[name] = arg_type.conv_to_ds(val)
        index = self._objs.index(obj)
        self._dyn_arr.edit_instance(index, values)

    def __getitem__(self, key):
        if key >= self._dyn_arr.size:
            raise IndexError("Key is out of bounds! ", key)

        values = self._dyn_arr.get_instance(key)
        desc = _struct_desc[type(self._arg.value)]
        obj = self._objs[key]
        for name, arg_type in desc.fields:
            value = arg_type.conv_to_obj(values[name])
            setattr(obj, name, value)
        return obj

    def __len__(self):
        return self._dyn_arr.size

    def address(self):
        return self._dyn_arr.address_info()

    def address_off(self, obj):
        index = self._objs.index(obj)
        return self.address() + index * self.item_size

    @property
    def item_size(self):
        return self._dyn_arr.obj_size()

    @property
    def item_arg(self):
        return self._arg


class SimpleArray(Array):
    def __init__(self, item_size, reserve=0):
        self._reserve = reserve
        if reserve == 0:
            self._reserve = 1
        self._size = 0
        self._item_size = item_size
        self._address = x86.MemData(self._reserve*self._item_size)

    def __len__(self):
        return self._size

    def address(self):
        return self._address.ptr()

    @property
    def item_size(self):
        return self._item_size

    def _resize(self):
        if self._size >= 0 and self._size <= 100:
            self._reserve += 1
        elif self._size > 100 and self._size <= 10000:
            self._reserve += 100
        elif self._size > 10000 and self._size <= 1000000:
            self._reserve += 10000
        else:
            self._reserve += 100000

        temp = x86.MemData(self._item_size*self._reserve)
        memcpy(temp.ptr(), self._address.ptr(), self._size*self._item_size)
        self._address = temp

    def __getitem__(self, key):
        if key >= self._size:
            raise IndexError("Key is out of bounds! ", key)

        offset = self._item_size * key
        adr = self._address.ptr() + offset
        return self._get_item(adr)

    def __setitem__(self, key, value):
        if key >= self._size:
            raise IndexError("Key is out of bounds! ", key)

        offset = self._item_size * key
        adr = self._address.ptr() + offset
        self._set_item(adr, value)

    def append(self, value):
        if self._reserve == self._size:
            self._resize()

        offset = self._item_size * self._size
        adr = self._address.ptr() + offset
        self._set_item(adr, value)
        self._size += 1

    def extend(self, values):
        for v in values:
            self.append(v)

    def resize(self, new_size):
        if new_size > self._size:
            if new_size > self._reserve:
                self._reserve = new_size
                temp = x86.MemData(self._item_size*self._reserve)
                memcpy(temp.ptr(), self._address.ptr(), self._size*self._item_size)
                self._address = temp
                self._size = new_size
            else:
                self._size = new_size
        elif new_size < self._size:
            self._size = new_size

    def _get_item(self, address):
        raise NotImplementedError()

    def _set_item(self, address, value):
        raise NotImplementedError()


class PtrsArray(SimpleArray):
    def __init__(self, reserve=0):
        self.BIT64 = False
        item_size = 4
        bits = platform.architecture()[0]
        if bits == '64bit':
            item_size = 8
            self.BIT64 = True
        super(PtrsArray, self).__init__(item_size, reserve)

    def _get_item(self, address):
        if self.BIT64:
            return x86.GetUInt64(address, 0, 0)
        else:
            return x86.GetUInt32(address, 0, 0)

    def _set_item(self, address, value):
        if not isinstance(value, int):
            raise TypeError("Only ints are accepted in PtrsArray", value)
        if self.BIT64:
            x86.SetUInt64(address, value, 0)
        else:
            x86.SetUInt32(address, value, 0)


class IntArray(SimpleArray):
    def __init__(self, reserve=0):
        super(IntArray, self).__init__(4, reserve)

    def _get_item(self, address):
        return x86.GetUInt32(address, 0, 0)

    def _set_item(self, address, value):
        if not isinstance(value, int):
            raise TypeError("Only ints are accepted in IntArray", value)
        x86.SetUInt32(address, value, 0)


class FloatArray(SimpleArray):
    def __init__(self, reserve=0):
        super(FloatArray, self).__init__(4, reserve)

    def _get_item(self, address):
        return x86.GetFloat(address, 0, 0)

    def _set_item(self, address, value):
        if not isinstance(value, float):
            raise TypeError("Only floats are accepted in FloatArray", value)
        x86.SetFloat(address, value, 0)


class Vec4Array(SimpleArray):
    def __init__(self, reserve=0):
        super(Vec4Array, self).__init__(16, reserve)

    def _get_item(self, address):
        return x86.GetFloat(address, 0, 4)

    def _set_item(self, address, value):
        if not isinstance(value, tuple):
            raise TypeError("Only tuple of floats are accepted in Vec4Array", value)
        x86.SetFloat(address, value, 0)


class ArrayArg(Argument):
    def __init__(self, name, value, stack_array=False):
        super(ArrayArg, self).__init__(name)
        assert isinstance(value, Array)
        self._value = value
        self._stack_array = stack_array

    @property
    def stack_array(self):
        return self._stack_array

    def _set_value(self, value):
        assert isinstance(value, Array)
        if isinstance(self._value, ObjArray) and isinstance(value, ObjArray):
            if self._value.item_arg.type_name != value.item_arg.type_name:
                raise TypeError("ObjArray mismatch for ArrayArg!", self._value, value)
        assert type(self._value) is type(value)
        self._value = value

    def _get_value(self):
        return self._value
    value = property(_get_value, _set_value)

    def update(self, ds, path=None):
        ptr = self._value.address()
        if path is None:
            ds[self.name] = ptr
        else:
            ds[path + self.name] = ptr

    def generate_data(self, cgen):
        if cgen.BIT64:
            return 'uint64 %s\n' % self.name
        else:
            return 'uint32 %s\n' % self.name

    def stack_size(self):
        return self._value.item_size * len(self._value)

    def from_ds(self, ds, path=None):
        return self._value

    @classmethod
    def conv_to_ds(cls, obj):
        return obj.address()
